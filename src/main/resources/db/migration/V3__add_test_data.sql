/* operators */
insert into operators (sex, address, birthday, first_name, last_name, mob_phone)
values ('M', 'Golosiivska str', '1992-07-22', 'Sviatoslav', 'Sviat', '3459201');
insert into operators (sex, address, birthday, first_name, last_name, mob_phone)
values ('F', 'Ahmatova str', '1995-02-11', 'Olga', 'Kovalenko', '5729955');
insert into operators (sex, address, birthday, first_name, last_name, mob_phone)
values ('F', 'Naberezhna str', '2001-11-05', 'Kateryna', 'Pavlenko', '5725782');


/* sellers */
insert into sellers (city, name, phone_number, post_index)
values ('Kyiv', 'Citrus', '11117777', 02075);
insert into sellers (city, name, phone_number, post_index)
values ('Lviv', 'Moyo', '22229999', 01012);
insert into sellers (city, name, phone_number, post_index)
values ('Luck', 'Rozetka', '55553333', 05001);


/* warehouses */
insert into warehouses (city, country, name, phone_number, post_index)
values ('Kyiv', 'Ukraine', 'warehouse-1', '1233211', 01093);
insert into warehouses (city, country, name, phone_number, post_index)
values ('Lviv', 'Ukraine', 'warehouse-2', '4566544', 01015);


/* customers */
insert into customers (age, first_name, last_name, money, user_id)
values (25, 'Andriy', 'Vasylenko', 10000, null);


/* discounts */
insert into discounts (percentage) values (15);
insert into discounts (percentage) values (35);


/* items */
insert into items (created_date, description, modified_date, price, quantity, title, operator_id, seller_id, warehouse_id)
values ('2020-08-21', 'mob phone', '2020-8-21', 2500, 7, 'OnePlus 8', 1, 1, 1);
insert into items (created_date, description, modified_date, price, quantity, title, operator_id, seller_id, warehouse_id)
values ('2020-08-23', 'mob phone', '2020-8-23', 3000, 6, 'OnePlus 8 pro', 2, 3, 1);
insert into items (created_date, description, modified_date, price, quantity, title, operator_id, seller_id, warehouse_id)
values ('2020-08-26', 'notebook', '2020-8-26', 3500, 5, 'MSI', 3, 2, 2);
insert into items (created_date, description, modified_date, price, quantity, title, operator_id, seller_id, warehouse_id)
values ('2020-08-22', 'TV', '2020-8-22', 5500, 11, 'Samsung QE558500', 2, 1, 1);
insert into items (created_date, description, modified_date, price, quantity, title, operator_id, seller_id, warehouse_id)
values ('2020-08-21', 'game console', '2020-8-21', 1800, 22, 'PS 5', 3, 2, 2);


/* connect discounts with items */
insert into discounts_items (discount_entity_id, items_id)
values (1, 5);
insert into discounts_items (discount_entity_id, items_id)
values (2, 4);