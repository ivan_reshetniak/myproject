insert into t_role (id, name) values (1, 'ROLE_USER');
insert into t_role (id, name) values (2, 'ROLE_OPERATOR');
insert into t_role (id, name) values (3, 'ROLE_CUSTOMER');
insert into t_role (id, name) values (4, 'ROLE_ADMIN');