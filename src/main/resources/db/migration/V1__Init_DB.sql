
create table all_users (id int8 generated by default as identity, password varchar(255), username varchar(255), primary key (id));
create table all_users_roles (user_id int8 not null, roles_id int8 not null, primary key (user_id, roles_id));
create table customers (id int8 generated by default as identity, age int4 not null, first_name varchar(255), last_name varchar(255), money int4 not null, user_id int8, primary key (id));
create table discounts (id int8 generated by default as identity, percentage int4 not null, primary key (id));
create table discounts_items (discount_entity_id int8 not null, items_id int8 not null);
create table items (id int8 generated by default as identity, created_date timestamp, description varchar(255), modified_date timestamp, price int4 not null, quantity int4 not null, title varchar(255), operator_id int8 not null, seller_id int8 not null, warehouse_id int8 not null, primary key (id));
create table operators (id int8 generated by default as identity, sex varchar(255), address varchar(255), birthday timestamp, first_name varchar(255), last_name varchar(255), mob_phone varchar(255), primary key (id));
create table sellers (id int8 generated by default as identity, city varchar(255), name varchar(255), phone_number varchar(255), post_index int4 not null, primary key (id));
create table t_role (id int8 generated by default as identity, name varchar(255), primary key (id));
create table warehouses (id int8 generated by default as identity, city varchar(255), country varchar(255), name varchar(255), phone_number varchar(255), post_index int4 not null, primary key (id));
alter table if exists discounts_items add constraint UK_6uekbhrxwl7wt189tqd0svon2 unique (items_id);
alter table if exists all_users_roles add constraint FK3e9ftqm3omjb5q2o73i3xuwaq foreign key (roles_id) references t_role;
alter table if exists all_users_roles add constraint FKoji6pr3r145oppgyfwelj8hps foreign key (user_id) references all_users;
alter table if exists customers add constraint FKg8g6ay3tixtckip1vqyqpf38n foreign key (user_id) references all_users;
alter table if exists discounts_items add constraint FKm2qy39ocx5awanoa21wfpb6j2 foreign key (items_id) references items;
alter table if exists discounts_items add constraint FKkaosfxkp1vdl9kkfw0vcaracg foreign key (discount_entity_id) references discounts;
alter table if exists items add constraint FK48cijum0wy3vjbst2vg8ej89p foreign key (operator_id) references operators;
alter table if exists items add constraint FKcypsmgax0upa6oio1b6cdx5kl foreign key (seller_id) references sellers;
alter table if exists items add constraint FKesjtuiq3dsdin8j637ofumv6v foreign key (warehouse_id) references warehouses;