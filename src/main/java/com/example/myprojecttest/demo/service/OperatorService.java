package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.converter.OperatorConverter;
import com.example.myprojecttest.demo.dto.OperatorDto;
import com.example.myprojecttest.demo.entity.OperatorEntity;
import com.example.myprojecttest.demo.repo.OperatorRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OperatorService {

    private OperatorRepo repo;
    private OperatorConverter operatorConverter;

    public OperatorService(OperatorRepo operatorRepo, OperatorConverter operatorConverter) {
        this.repo = operatorRepo;
        this.operatorConverter = operatorConverter;
    }

    public List <OperatorDto> getOperators() {
        List<OperatorEntity> operatorEntities =  repo.findAll();

        return operatorEntities.stream()
                .map(operatorConverter::toDto)
                .collect(Collectors.toList());
    }

    public OperatorDto getOperatorByLastName(String lastName){
        OperatorEntity operatorEntity = repo.findFirstByLastName(lastName).orElseThrow(() -> new RuntimeException("Operator not found"));

        return operatorConverter.toDto(operatorEntity);
    }

    public OperatorDto createOperator (OperatorDto dto){

        OperatorEntity toAdd = new OperatorEntity();
        toAdd.setFirstName(dto.getFirstName());
        toAdd.setLastName(dto.getLastName());
        toAdd.setAddress(dto.getAddress());
        toAdd.setSex(dto.getSex());
        toAdd.setBirthday(dto.getBirthday());
        toAdd.setMobPhone(dto.getMobPhone());

        repo.save(toAdd);
        return dto;
    }

    public void deleteOperator (long id){
        OperatorEntity current = repo.findById(id)
                .orElseThrow(() -> new RuntimeException("Operator not found"));

        repo.delete(current);
    }
}
