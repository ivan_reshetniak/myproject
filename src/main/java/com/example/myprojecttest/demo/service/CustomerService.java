package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.domain.User;
import com.example.myprojecttest.demo.domain.UserService;
import com.example.myprojecttest.demo.entity.CustomerEntity;
import com.example.myprojecttest.demo.repo.CustomerRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerService {

    private CustomerRepo repo;
    private UserService userService;


    public CustomerEntity addMoney(long id, int money){
        CustomerEntity current = repo.findById(id).orElseThrow(()->new RuntimeException("Customer not found"));

        int currentMoney = current.getMoney();
        current.setMoney(currentMoney + money);
        repo.save(current);
        return current;
    }

    public List<CustomerEntity> getAllCustomers(){
        return repo.findAll();
    }

    public CustomerEntity createCustomer (CustomerEntity entity){
        repo.save(entity);
        return entity;
    }

    public List<CustomerEntity> getCustomerByUsername(String username){
        List<CustomerEntity> customerEntities = repo.getCustomerEntitiesByUserAndUserName(username);
        if (!customerEntities.isEmpty()){
            return customerEntities;
        }
        throw new RuntimeException("Customer(s) not found");
    }

    public boolean connectCustomerWithUser(long customerId, String userName) {
        CustomerEntity current = repo.findById(customerId).orElseThrow(() ->new RuntimeException("Customer not found"));
        User currentUser = (User) userService.loadUserByUsername(userName);

        if (!currentUser.getRoles().stream().filter(x -> x.getName().equals("ROLE_CUSTOMER")).findFirst().equals(Optional.empty())){
            current.setUser(currentUser);
            repo.save(current);
            return true;
        }
        return false;
    }
}