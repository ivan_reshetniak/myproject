package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.converter.DiscountConverter;
import com.example.myprojecttest.demo.dto.DiscountDto;
import com.example.myprojecttest.demo.entity.DiscountEntity;
import com.example.myprojecttest.demo.entity.ItemEntity;
import com.example.myprojecttest.demo.repo.DiscountRepo;
import com.example.myprojecttest.demo.repo.ItemRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DiscountService {
    private DiscountRepo discountRepo;
    private DiscountConverter discountConverter;
    private ItemRepo itemRepo;


    public List<DiscountDto> getDiscounts() {
        List<DiscountEntity> discountEntities = discountRepo.findAll();

        return discountEntities.stream()
                .map(x -> discountConverter.toDto(x))
                .collect(Collectors.toList());
    }

    public DiscountDto createDiscount (int percentage) {
        DiscountEntity entityToAdd = new DiscountEntity();

        entityToAdd.setPercentage(percentage);
        discountRepo.save(entityToAdd);

        return discountConverter.toDto(entityToAdd);
    }

    public DiscountDto connectDiscountWithItem (long discountId, long itemId) {
        DiscountEntity entity = discountRepo.findById(discountId).orElseThrow(() -> new RuntimeException("Discount not found"));
        ItemEntity itemEntity = itemRepo.findById(itemId).orElseThrow(() -> new RuntimeException("Item not found"));

        entity.getItems().add(itemEntity);
        discountRepo.save(entity);

        return discountConverter.toDto(entity);
    }

    public DiscountDto getDiscountByItemsId (long itemId) {
        DiscountEntity entity = discountRepo.findByItemsId(itemId).orElseThrow(() -> new RuntimeException("Discount not found"));

        return discountConverter.toDto(entity);
    }
}
