package com.example.myprojecttest.demo.service;


import com.example.myprojecttest.demo.converter.WarehouseConverter;
import com.example.myprojecttest.demo.dto.WarehouseDto;
import com.example.myprojecttest.demo.entity.WarehouseEntity;
import com.example.myprojecttest.demo.repo.WarehouseRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WarehouseService {

    private WarehouseRepo repo;
    private WarehouseConverter warehouseConverter;

    public WarehouseService(WarehouseRepo warehouseRepo, WarehouseConverter warehouseConverter) {
        this.repo = warehouseRepo;
        this.warehouseConverter = warehouseConverter;
    }

    public List<WarehouseDto> getWarehouses(){
        List<WarehouseEntity> warehouseEntities = repo.findAll();

        return warehouseEntities.stream()
                .map(warehouseConverter::toDto)
                .collect(Collectors.toList());
    }

    public WarehouseDto getWarehouseByName (String name){
        WarehouseEntity warehouseEntity = repo.findFirstByName(name).orElseThrow(() -> new RuntimeException("Warehouse is not exist!"));

        return warehouseConverter.toDto(warehouseEntity);
    }


    public WarehouseDto updateWarehouse (long id, WarehouseDto updatedWarehouse){
        WarehouseEntity warehouseEntity = repo.findById(id).orElseThrow(RuntimeException::new);

        if (updatedWarehouse.getName() != null && !updatedWarehouse.getName().equals(warehouseEntity.getName())){
            warehouseEntity.setName(updatedWarehouse.getName());
        }
        if (updatedWarehouse.getCountry() != null && !updatedWarehouse.getCountry().equals(warehouseEntity.getCountry())){
            warehouseEntity.setCountry(updatedWarehouse.getCountry());
        }
        if (updatedWarehouse.getCity() != null && !updatedWarehouse.getCity().equals(warehouseEntity.getCity())){
            warehouseEntity.setCity(updatedWarehouse.getCity());
        }
        if (updatedWarehouse.getPostIndex() > 0 && updatedWarehouse.getPostIndex() != warehouseEntity.getPostIndex()){
            warehouseEntity.setPostIndex(updatedWarehouse.getPostIndex());
        }
        if (updatedWarehouse.getPhoneNumber() != null && !updatedWarehouse.getPhoneNumber().equals(warehouseEntity.getPhoneNumber())){
            warehouseEntity.setPhoneNumber(updatedWarehouse.getPhoneNumber());
        }

        repo.save(warehouseEntity);

        return warehouseConverter.toDto(warehouseEntity);
    }

    public void deleteWarehouse (long id) {

        WarehouseEntity current = repo.findById(id)
                .orElseThrow(() -> new RuntimeException("Warehouse not found"));

        repo.delete(current);
    }

    public WarehouseDto createWarehouse (WarehouseDto dto){

        if (repo.findFirstByName(dto.getName()).isPresent()){
            throw new RuntimeException("Warehouse is exists!");
        }

        WarehouseEntity toAdd = new WarehouseEntity();

        toAdd.setName(dto.getName());
        toAdd.setCountry(dto.getCountry());
        toAdd.setCity(dto.getCity());
        toAdd.setPhoneNumber(dto.getPhoneNumber());
        toAdd.setPostIndex(dto.getPostIndex());

        repo.save(toAdd);
        dto.setId(toAdd.getId());
        return dto;
    }
}
