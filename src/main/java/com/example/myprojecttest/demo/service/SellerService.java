package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.converter.SellerConverter;
import com.example.myprojecttest.demo.dto.SellerDto;
import com.example.myprojecttest.demo.entity.SellerEntity;
import com.example.myprojecttest.demo.repo.SellerRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SellerService {

    private SellerRepo repo;
    private SellerConverter sellerConverter;

    public SellerService(SellerRepo sellerRepo, SellerConverter sellerConverter) {
        this.repo = sellerRepo;
        this.sellerConverter = sellerConverter;
    }

    public List <SellerDto> getSellers() {
        List<SellerEntity> sellerEntities =  repo.findAll();

        return sellerEntities.stream()
                .map(sellerConverter::toDto)
                .collect(Collectors.toList());
    }

    public SellerDto getSellerByName (String name){
        SellerEntity sellerEntity = repo.getFirstByName(name).orElseThrow(() -> new RuntimeException("Seller not found"));

        return sellerConverter.toDto(sellerEntity);
    }

    public void deleteSeller (long id){
        SellerEntity current = repo.findById(id)
                .orElseThrow(() -> new RuntimeException("Seller not found"));

        repo.delete(current);
    }

    public SellerDto createSeller (SellerDto dto){
        if (repo.getFirstByName(dto.getName()).isPresent()){
            throw new RuntimeException("Seller already exists!");
        }

        SellerEntity toAdd = new SellerEntity();
        toAdd.setName(dto.getName());
        toAdd.setCity(dto.getCity());
        toAdd.setPostIndex(dto.getPostIndex());
        toAdd.setPhoneNumber(dto.getPhoneNumber());

        repo.save(toAdd);
        return dto;
    }
}
