package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.converter.ItemConverter;
import com.example.myprojecttest.demo.dto.ItemDto;
import com.example.myprojecttest.demo.entity.CustomerEntity;
import com.example.myprojecttest.demo.entity.DiscountEntity;
import com.example.myprojecttest.demo.entity.ItemEntity;
import com.example.myprojecttest.demo.form.ItemForm;
import com.example.myprojecttest.demo.repo.CustomerRepo;
import com.example.myprojecttest.demo.repo.DiscountRepo;
import com.example.myprojecttest.demo.repo.ItemRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ItemService {

    private ItemRepo repo;
    private ItemConverter itemConverter;
    private CustomerRepo customerRepo;
    private DiscountRepo discountRepo;
    private OperatorService operatorService;
    private WarehouseService warehouseService;
    private SellerService sellerService;


    public List <ItemDto> getItems() {
        List <ItemEntity> itemEntities = repo.findAll();

        return itemEntities.stream()
                .map(itemConverter::toDto)
                .collect(Collectors.toList());
    }

    public List <ItemDto> getItemsByOperator (String lastName) {
        List<ItemEntity> itemEntities = repo.findAllByOperatorAndLastName(lastName);

        return itemEntities.stream()
                .map(itemConverter::toDto)
                .collect(Collectors.toList());
    }

    public ItemDto createItem (ItemForm createItemForm){
        ItemDto dto = new ItemDto();
        dto.setTitle(createItemForm.getTitle());
        dto.setDescription(createItemForm.getDescription());
        dto.setOperator(operatorService.getOperatorByLastName(createItemForm.getOperatorLastName()));
        dto.setQuantity(createItemForm.getQuantity());
        dto.setWarehouse(warehouseService.getWarehouseByName(createItemForm.getWarehouseName()));
        dto.setCreatedDate(LocalDateTime.now());
        dto.setModifiedDate(LocalDateTime.now());
        dto.setSeller(sellerService.getSellerByName(createItemForm.getSellerName()));
        dto.setPrice(createItemForm.getPrice());

        ItemEntity toAdd = itemConverter.toEntity(dto);
        repo.save(toAdd);
        return dto;
    }

    public ItemDto buyItem (long customerId, long itemId, int quantity){
        ItemEntity item = repo.findById(itemId).orElseThrow(()->new RuntimeException("Item not found"));
        CustomerEntity currentCustomer = customerRepo.findById(customerId).orElseThrow(() -> new RuntimeException("Customer not found"));
        DiscountEntity discountEntity = discountRepo.findByItemsId(itemId).orElse(null);

        int discount = 100;
        if (discountEntity != null){
            discount = discount - discountEntity.getPercentage();
        }

        if (item.getQuantity() - quantity < 0){
            throw new RuntimeException("Not enough items");
        }
        if (currentCustomer.getMoney() - item.getPrice() * quantity < 0){
            throw new RuntimeException("Not enough money");
        }

        item.setQuantity(item.getQuantity() - quantity);
        repo.save(item);

        ItemDto dto = itemConverter.toDto(item);
        dto.setQuantity(quantity);

        currentCustomer.setMoney(currentCustomer.getMoney() - (item.getPrice() * discount / 100  * quantity));
        customerRepo.save(currentCustomer);
        return dto;
    }
}
