package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.dto.ItemDto;
import com.example.myprojecttest.demo.form.ItemForm;
import com.example.myprojecttest.demo.service.ItemService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping (value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)


public class ItemController {
    private ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }


    @GetMapping("items")
    public List<ItemDto> getItems() {
        return itemService.getItems();
    }


    @GetMapping("items/operator/{lastName}")
    public List <ItemDto> getItemsByOperator (@PathVariable String lastName) {
        return itemService.getItemsByOperator(lastName);
    }

    @PostMapping("items")
    public ItemDto createItem (@RequestBody ItemForm createItemForm){
        return itemService.createItem(createItemForm);
    }

    @PutMapping("items")
    public ItemDto buyItems (@RequestBody Map<String, Long> params){
        return itemService.buyItem(params.get("customerId"), params.get("itemId"), params.get("quantity").intValue());
    }
}
