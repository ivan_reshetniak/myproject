package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.entity.CustomerEntity;
import com.example.myprojecttest.demo.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping (value = "/api/customer", produces = MediaType.APPLICATION_JSON_VALUE)

@AllArgsConstructor
public class CustomerController {
    private CustomerService customerService;


    @PutMapping
    public CustomerEntity addMoney (@RequestBody Map<String, Long> params){

        return customerService.addMoney(params.get("customerId"), params.get("money").intValue());
    }

    @GetMapping
    public List<CustomerEntity> getAllCustomers(){
        return customerService.getAllCustomers();
    }

    @PostMapping
    public CustomerEntity createCustomer (@RequestBody CustomerEntity customer){
        return customerService.createCustomer(customer);
    }

    @PutMapping("/connect")
    public boolean connectCustomerWithUser (@RequestBody Map<String, String> params){
        return customerService.connectCustomerWithUser(Long.parseLong(params.get("customerId")), params.get("userName"));
    }

    @GetMapping("/{userName}")
    public List<CustomerEntity> getCustomerByUsername (@PathVariable String userName){
        return customerService.getCustomerByUsername(userName);
    }
}
