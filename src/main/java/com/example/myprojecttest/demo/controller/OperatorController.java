package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.dto.OperatorDto;
import com.example.myprojecttest.demo.service.OperatorService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping (value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class OperatorController {
    private OperatorService operatorService;

    public OperatorController(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

    @GetMapping("operators")
    public List<OperatorDto> getOperators(){
        return operatorService.getOperators();
    }

    @GetMapping("operator/{lastName}")
    public OperatorDto getOperatorByLastName(@PathVariable String lastName){
        return operatorService.getOperatorByLastName(lastName);
    }

    @PostMapping("operator")
    public OperatorDto createOperator (@RequestBody OperatorDto dto){
        return operatorService.createOperator(dto);
    }

    @DeleteMapping("delete/operator/{id}")
    public void deleteOperator (@PathVariable long id){
        operatorService.deleteOperator(id);
    }
}
