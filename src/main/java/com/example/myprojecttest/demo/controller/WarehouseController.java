package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.dto.WarehouseDto;
import com.example.myprojecttest.demo.service.WarehouseService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class WarehouseController {

    private WarehouseService warehouseService;

    public WarehouseController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @GetMapping("warehouses")
    public List<WarehouseDto> getWarehouses(){
        return warehouseService.getWarehouses();
    }

    @GetMapping("warehouse/{name}")
    public WarehouseDto getWarehouseByName (@PathVariable String name){
        return  warehouseService.getWarehouseByName(name);
    }

    @PutMapping("warehouse/{id}")
    public WarehouseDto updateWarehouse (@PathVariable long id, WarehouseDto warehouseDto){
        return warehouseService.updateWarehouse(id, warehouseDto);
    }

    @DeleteMapping("delete/warehouse/{id}")
    public void deleteWarehouse (@PathVariable long id){
        warehouseService.deleteWarehouse(id);
    }

    @PostMapping("warehouse")
    public WarehouseDto createWarehouse (@RequestBody WarehouseDto dto){
        return warehouseService.createWarehouse(dto);
    }
}
