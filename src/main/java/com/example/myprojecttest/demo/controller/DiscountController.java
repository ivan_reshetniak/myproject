package com.example.myprojecttest.demo.controller;


import com.example.myprojecttest.demo.dto.DiscountDto;
import com.example.myprojecttest.demo.service.DiscountService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/discounts", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class DiscountController {
    private DiscountService discountService;


    @GetMapping
    public List<DiscountDto> getDiscounts() {
        return discountService.getDiscounts();
    }

    @PostMapping
    public DiscountDto createDiscount(@RequestBody Map<String, Integer> params) {
        return discountService.createDiscount(params.get("percentage"));
    }

    @PutMapping
    public DiscountDto connectDiscountWithItem(@RequestBody Map<String, Long> params) {
        return discountService.connectDiscountWithItem(params.get("discountId"), params.get("itemId"));
    }

    @GetMapping("{itemId}")
    public DiscountDto getDiscountByItemsId (@PathVariable long itemId) {
        return discountService.getDiscountByItemsId(itemId);
    }
}
