package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.domain.User;
import com.example.myprojecttest.demo.domain.UserService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping (value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user")
    public boolean saveUser (@RequestBody User user){
        return userService.saveUser(user);
    }

    @GetMapping("/users")
    public List<User> users() {
        return userService.getUsers();
    }

    @PostMapping("/user/operator")
    public boolean saveUserOperator (@RequestBody User user){
        return userService.saveUserOperator(user);
    }

    @PostMapping("/user/customer")
    public boolean saveUserCustomer (@RequestBody User user){
        return userService.saveUserCustomer(user);
    }
}
