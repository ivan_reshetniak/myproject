package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.dto.SellerDto;
import com.example.myprojecttest.demo.service.SellerService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping (value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class SellerController {

    private SellerService sellerService;

    public SellerController(SellerService sellerService) {
        this.sellerService = sellerService;
    }

    @GetMapping("sellers")
    public List<SellerDto> getSellers() {
        return sellerService.getSellers();
    }

    @GetMapping("seller/{name}")
    public SellerDto getSellerByName (@PathVariable String name){
        return sellerService.getSellerByName(name);
    }

    @PostMapping("seller")
    public SellerDto createSeller (@RequestBody SellerDto dto){
        return sellerService.createSeller(dto);
    }

    @DeleteMapping("delete/seller/{id}")
    public void deleteSeller (@PathVariable long id){
        sellerService.deleteSeller(id);
    }
}
