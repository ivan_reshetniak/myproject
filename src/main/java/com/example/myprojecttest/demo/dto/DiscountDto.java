package com.example.myprojecttest.demo.dto;

import lombok.*;

import java.util.List;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DiscountDto {
    private long id;
    private int percentage;
    private List<ItemDto> items;
}
