package com.example.myprojecttest.demo.dto;

import lombok.*;


import java.io.Serializable;
import java.time.LocalDateTime;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ItemDto implements Serializable {


    private long id;
    private String title;
    private String description;
    private OperatorDto operator;
    private int quantity;
    private WarehouseDto warehouse;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private SellerDto seller;
    private int price;
}
