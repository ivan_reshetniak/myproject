package com.example.myprojecttest.demo.dto;

import lombok.*;

import java.io.Serializable;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SellerDto implements Serializable {

    long id;
    private String name;
    private String city;
    private int postIndex;
    private String phoneNumber;
}
