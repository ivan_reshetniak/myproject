package com.example.myprojecttest.demo.dto;

import lombok.*;

import java.io.Serializable;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class WarehouseDto implements Serializable {

    long id;
    private String name;
    private String country;
    private String city;
    private int postIndex;
    private String phoneNumber;
}
