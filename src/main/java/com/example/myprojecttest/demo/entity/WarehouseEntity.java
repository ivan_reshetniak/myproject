package com.example.myprojecttest.demo.entity;


import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "warehouses")
public class WarehouseEntity {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) long id;
    private String name;
    private String country;
    private String city;
    private int postIndex;
    private String phoneNumber;

//    @OneToMany (mappedBy = "warehouse", fetch = FetchType.EAGER)
//    private List <ItemEntity> items = new ArrayList<>();
}
