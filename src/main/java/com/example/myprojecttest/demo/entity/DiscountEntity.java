package com.example.myprojecttest.demo.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table (name = "discounts")
public class DiscountEntity {
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY) long id;
    private int percentage;

    @OneToMany (fetch = FetchType.EAGER)
    private List<ItemEntity> items;
}
