package com.example.myprojecttest.demo.entity;

import com.example.myprojecttest.demo.domain.User;
import lombok.*;

import javax.persistence.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table (name = "customers")
public class CustomerEntity {
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY) long id;
    private String firstName;
    private String lastName;
    private int age;
    @OneToOne
    @JoinColumn (name = "user_id")
    private User user;
    private int money;
}
