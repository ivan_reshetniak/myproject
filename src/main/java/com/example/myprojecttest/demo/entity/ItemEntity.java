package com.example.myprojecttest.demo.entity;


import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table (name = "items")
public class ItemEntity {
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY) long id;
    private String title;
    private String description;

    @ManyToOne (optional = false)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn (name = "operator_id")
    private OperatorEntity operator;

    private int quantity;

    @ManyToOne (optional = false)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn (name = "warehouse_id")
    private WarehouseEntity warehouse;

    private LocalDateTime createdDate;

    private LocalDateTime modifiedDate;

    @ManyToOne (optional = false)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn (name = "seller_id")
    private SellerEntity seller;

    private int price;
}
