package com.example.myprojecttest.demo.entity;

import lombok.*;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table (name = "operators")
public class OperatorEntity {
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY) long id;
    private String firstName;
    private String lastName;
    private String Sex;
    private LocalDateTime birthday;
    private String mobPhone;
    private String address;

//    @OneToMany (mappedBy = "operator", fetch = FetchType.EAGER)
//    private List <ItemEntity> items = new ArrayList<>();
}
