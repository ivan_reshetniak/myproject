package com.example.myprojecttest.demo.form;

import lombok.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ItemForm {

    private String title;
    private String description;
    private String operatorLastName;
    private int quantity;
    private String warehouseName;
    private String sellerName;
    private int price;
}
