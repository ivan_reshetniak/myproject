package com.example.myprojecttest.demo.repo;

import com.example.myprojecttest.demo.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepo extends JpaRepository<CustomerEntity, Long> {
    @Query (value = "select * from customers inner join all_users on customers.user_id = all_users.id where username = ?1",
    nativeQuery = true)
    List<CustomerEntity> getCustomerEntitiesByUserAndUserName (String username);
}
