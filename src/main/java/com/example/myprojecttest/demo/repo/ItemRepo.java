package com.example.myprojecttest.demo.repo;


import com.example.myprojecttest.demo.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepo extends JpaRepository <ItemEntity, Long> {

    @Query (value = "select * from items inner join operators on items.operator_id = operators.id where last_name = ?1", nativeQuery = true)
    List <ItemEntity> findAllByOperatorAndLastName (String lastName);
}
