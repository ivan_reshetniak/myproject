package com.example.myprojecttest.demo.repo;

import com.example.myprojecttest.demo.entity.DiscountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DiscountRepo extends JpaRepository<DiscountEntity, Long> {
    Optional<DiscountEntity> findByItemsId (long itemId);
}
