package com.example.myprojecttest.demo.repo;


import com.example.myprojecttest.demo.entity.WarehouseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WarehouseRepo extends JpaRepository<WarehouseEntity, Long> {

    Optional<WarehouseEntity> findFirstByName (String name);
}
