package com.example.myprojecttest.demo.repo;


import com.example.myprojecttest.demo.entity.OperatorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OperatorRepo extends JpaRepository <OperatorEntity, Long> {

    Optional<OperatorEntity> findFirstByLastName (String lastName);
}
