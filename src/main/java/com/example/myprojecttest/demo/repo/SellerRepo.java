package com.example.myprojecttest.demo.repo;


import com.example.myprojecttest.demo.entity.SellerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SellerRepo extends JpaRepository <SellerEntity, Long> {

    Optional<SellerEntity> getFirstByName (String name);
}
