package com.example.myprojecttest.demo.testController;

import com.example.myprojecttest.demo.dto.ItemDto;
import com.example.myprojecttest.demo.dto.SellerDto;
import com.example.myprojecttest.demo.service.ItemService;
import com.example.myprojecttest.demo.service.OperatorService;
import com.example.myprojecttest.demo.service.SellerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(value = "test")
@AllArgsConstructor
public class TestController {
    private OperatorService operatorService;
    private ItemService itemService;
    private SellerService sellerService;


    @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
    public String index(Model model) {

        return "index";
    }


    @RequestMapping(value = "/items")
    public String itemList(Model model) {

        List<ItemDto> itemDtos = itemService.getItems();

        model.addAttribute("items", itemDtos);
        return "ItemList";
    }

    @RequestMapping(value = "/sellers")
    public String sellerList(Model model) {

        List<SellerDto> sellers = sellerService.getSellers();
        model.addAttribute("sellers", sellers);

        return "sellerList";
    }
}
