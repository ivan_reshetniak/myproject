package com.example.myprojecttest.demo.testController;

import com.example.myprojecttest.demo.dto.OperatorDto;
import com.example.myprojecttest.demo.service.OperatorService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "test/operators")
@AllArgsConstructor
public class OperatorViewController {
    private OperatorService operatorService;


    @RequestMapping(method = RequestMethod.GET)
    public String operatorList(Model model) {

        List<OperatorDto> operators = operatorService.getOperators();
        model.addAttribute("operators", operators);

        return "operatorList";
    }

    @GetMapping("/addOperator")
    public String showAddOperatorPage (Model model){

        OperatorDto operatorDto = new OperatorDto();
        model.addAttribute("operatorDto", operatorDto);
        return "addOperator";
    }


    @PostMapping("/addOperator")
    public String saveOperator (Model model,
                                @ModelAttribute("operatorDto") OperatorDto operatorDto){

        operatorService.createOperator(operatorDto);
        return "redirect:/test/operators";
    }
    
}
