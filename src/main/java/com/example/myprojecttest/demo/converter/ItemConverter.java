package com.example.myprojecttest.demo.converter;

import com.example.myprojecttest.demo.dto.ItemDto;
import com.example.myprojecttest.demo.entity.ItemEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ItemConverter {

    public ItemEntity toEntity(ItemDto dto) {
        return ItemEntity.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .operator(new OperatorConverter().toEntity(dto.getOperator()))
                .quantity(dto.getQuantity())
                .warehouse(new WarehouseConverter().toEntity(dto.getWarehouse()))
                .createdDate(dto.getCreatedDate())
                .modifiedDate(dto.getModifiedDate())
                .seller(new SellerConverter().toEntity(dto.getSeller()))
                .price(dto.getPrice())
                .build();
    }


    public ItemDto toDto(ItemEntity entity) {
        return ItemDto.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .description(entity.getDescription())
                .operator(new OperatorConverter().toDto(entity.getOperator()))
                .quantity(entity.getQuantity())
                .warehouse(new WarehouseConverter().toDto(entity.getWarehouse()))
                .createdDate(entity.getCreatedDate())
                .modifiedDate(entity.getModifiedDate())
                .seller(new SellerConverter().toDto(entity.getSeller()))
                .price(entity.getPrice())
                .build();
    }


    public List<ItemDto> toDto(List<ItemEntity> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }


    public List<ItemEntity> toEntity(List<ItemDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
