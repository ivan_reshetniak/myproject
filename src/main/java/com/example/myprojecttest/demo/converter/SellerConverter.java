package com.example.myprojecttest.demo.converter;

import com.example.myprojecttest.demo.dto.SellerDto;
import com.example.myprojecttest.demo.entity.SellerEntity;
import org.springframework.stereotype.Component;

@Component
public class SellerConverter {

    public SellerEntity toEntity(SellerDto dto){
        return SellerEntity.builder()
                .id(dto.getId())
                .name(dto.getName())
                .city(dto.getCity())
                .postIndex(dto.getPostIndex())
                .phoneNumber(dto.getPhoneNumber())
                .build();
    }


    public SellerDto toDto(SellerEntity entity){
        return SellerDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .city(entity.getCity())
                .postIndex(entity.getPostIndex())
                .phoneNumber(entity.getPhoneNumber())
                .build();
    }
}
