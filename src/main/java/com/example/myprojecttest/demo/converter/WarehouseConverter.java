package com.example.myprojecttest.demo.converter;

import com.example.myprojecttest.demo.dto.WarehouseDto;
import com.example.myprojecttest.demo.entity.WarehouseEntity;
import org.springframework.stereotype.Component;

@Component
public class WarehouseConverter {

    public WarehouseEntity toEntity(WarehouseDto dto) {
        return WarehouseEntity.builder()
                .id(dto.getId())
                .name(dto.getName())
                .country(dto.getCountry())
                .city(dto.getCity())
                .postIndex(dto.getPostIndex())
                .phoneNumber(dto.getPhoneNumber())
                .build();
    }


    public WarehouseDto toDto(WarehouseEntity entity){
        return WarehouseDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .country(entity.getCountry())
                .city(entity.getCity())
                .postIndex(entity.getPostIndex())
                .phoneNumber(entity.getPhoneNumber())
                .build();
    }
}
