package com.example.myprojecttest.demo.converter;


import com.example.myprojecttest.demo.dto.OperatorDto;
import com.example.myprojecttest.demo.entity.OperatorEntity;
import org.springframework.stereotype.Component;

@Component
public class OperatorConverter {

    public OperatorEntity toEntity(OperatorDto dto) {
        return OperatorEntity.builder()
                .id(dto.getId())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .Sex(dto.getSex())
                .birthday(dto.getBirthday())
                .mobPhone(dto.getMobPhone())
                .address(dto.getAddress())
                .build();
    }


    public OperatorDto toDto(OperatorEntity entity){
        return OperatorDto.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .Sex(entity.getSex())
                .birthday(entity.getBirthday())
                .mobPhone(entity.getMobPhone())
                .address(entity.getAddress())
                .build();
    }
}
