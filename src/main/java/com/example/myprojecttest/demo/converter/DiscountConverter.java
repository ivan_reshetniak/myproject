package com.example.myprojecttest.demo.converter;


import com.example.myprojecttest.demo.dto.DiscountDto;
import com.example.myprojecttest.demo.entity.DiscountEntity;
import org.springframework.stereotype.Component;

@Component
public class DiscountConverter {

    public DiscountEntity toEntity(DiscountDto dto) {
        return DiscountEntity.builder()
                .id(dto.getId())
                .percentage(dto.getPercentage())
                .items(new ItemConverter().toEntity(dto.getItems()))
                .build();
    }


    public DiscountDto toDto(DiscountEntity entity) {

        if (entity.getItems() != null) {
            return DiscountDto.builder()
                    .id(entity.getId())
                    .percentage(entity.getPercentage())
                    .items(new ItemConverter().toDto(entity.getItems()))
                    .build();
        }else{
            return DiscountDto.builder()
                    .id(entity.getId())
                    .percentage(entity.getPercentage())
                    .build();
        }
    }
}
