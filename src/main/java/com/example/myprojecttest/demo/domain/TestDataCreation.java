package com.example.myprojecttest.demo.domain;

import com.example.myprojecttest.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TestDataCreation {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private UserService userService;


    @EventListener(ApplicationReadyEvent.class)
    public void addTestUsers() throws Exception {
        userService.createDefaultUsers();
        customerService.connectCustomerWithUser(1, "andrew");
    }
}
