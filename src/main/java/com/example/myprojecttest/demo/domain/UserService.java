package com.example.myprojecttest.demo.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {

    private UserRepo userRepo;
    private RoleRepo roleRepo;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    public UserService(UserRepo userRepo, RoleRepo roleRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);

        if (user == null){
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    public boolean saveUser(User user) {
        User userFromDB = userRepo.findByUsername(user.getUsername());

        if (userFromDB != null) {
            return false;
        }

        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepo.save(user);
        return true;
    }

    public List<User> getUsers() {
        return userRepo.findAll();
    }


    public boolean saveUserOperator(User user){
        User userFromDB = userRepo.findByUsername(user.getUsername());

        if (userFromDB != null){
            return false;
        }

        Set <Role> roles = new HashSet<>();
        roles.add(new Role(1L, "ROLE_USER"));
        roles.add(new Role(2L, "ROLE_OPERATOR"));

        user.setRoles(roles);

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepo.save(user);
        return true;
    }

    public boolean saveUserCustomer(User user){
        User userFromDB = userRepo.findByUsername(user.getUsername());

        if (userFromDB != null){
            return false;
        }

        Set <Role> roles = new HashSet<>();
        roles.add(new Role(1L,"ROLE_USER"));
        roles.add(new Role(3L,"ROLE_CUSTOMER"));

        user.setRoles(roles);

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepo.save(user);
        return true;
    }

    public boolean saveUserAdmin(User user){
        User userFromDB = userRepo.findByUsername(user.getUsername());

        if (userFromDB != null){
            return false;
        }

        Set <Role> roles = new HashSet<>();
        roles.add(new Role(1L,"ROLE_USER"));
        roles.add(new Role(2L,"ROLE_OPERATOR"));
        roles.add(new Role(3L,"ROLE_CUSTOMER"));
        roles.add(new Role(4L,"ROLE_ADMIN"));

        user.setRoles(roles);

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepo.save(user);
        return true;
    }

    public void createDefaultUsers() {
        //create default operator
        User defaultOperator = new User();
        defaultOperator.setUsername("yaryna");
        defaultOperator.setPassword("yaryna");
        saveUserOperator(defaultOperator);
        //create default customer
        User defaultCustomer = new User();
        defaultCustomer.setUsername("andrew");
        defaultCustomer.setPassword("andrew");
        saveUserCustomer(defaultCustomer);
        //create default user
        User defaultUser = new User();
        defaultUser.setUsername("iryna");
        defaultUser.setPassword("iryna");
        saveUser(defaultUser);
        //create admin
        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword("admin");
        saveUserAdmin(admin);
    }
}
