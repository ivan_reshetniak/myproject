package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.domain.Role;
import com.example.myprojecttest.demo.domain.User;
import com.example.myprojecttest.demo.domain.UserService;
import com.example.myprojecttest.demo.entity.CustomerEntity;
import com.example.myprojecttest.demo.repo.CustomerRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@SpringBootTest
@Transactional
class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private UserService userService;

    private CustomerEntity currentCustomer = CustomerEntity.builder().firstName("Vasyl").lastName("Vasylenko")
            .age(37).money(5500).user(null).build();


    @Test
    public void addMoneyTest() throws Exception {
        customerService.createCustomer(currentCustomer);
        CustomerEntity updatedCustomer = customerService.addMoney(currentCustomer.getId(), 1000);


        Assertions.assertEquals(6500, updatedCustomer.getMoney());
    }

    @Test
    void connectCustomerWithUserTest() throws Exception{
        customerService.createCustomer(currentCustomer);

        User currentUser = new User();
        currentUser.setUsername("username");
        currentUser.setPassword("password");
        userService.saveUserCustomer(currentUser);

        customerService.connectCustomerWithUser(currentCustomer.getId(), currentUser.getUsername());

        Assertions.assertEquals(currentCustomer.getUser().getUsername(), currentUser.getUsername());
    }

    @Test
    void connectCustomerWithUserFailTest() {
        customerService.createCustomer(currentCustomer);

        User currentUser = new User();
        currentUser.setUsername("username");
        currentUser.setPassword("password");
        userService.saveUserOperator(currentUser);

        Assertions.assertFalse(customerService.connectCustomerWithUser(currentCustomer.getId(), currentUser.getUsername()));
    }
}