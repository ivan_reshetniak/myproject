package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.dto.SellerDto;
import com.example.myprojecttest.demo.entity.SellerEntity;
import com.example.myprojecttest.demo.repo.SellerRepo;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SellerServiceTest {

    @Autowired
    private SellerService sellerService;
    @MockBean
    private SellerRepo sellerRepo;


    @BeforeEach
    public void setUp() {
        List<SellerEntity> sellerEntities = new ArrayList<>();
        SellerEntity test1 = SellerEntity.builder().id(1).city("Kyiv").name("Citrus").phoneNumber("1111")
                .postIndex(1111).build();
        SellerEntity test2 = SellerEntity.builder().id(2).city("Lviv").name("Moyo").phoneNumber("2222")
                .postIndex(2222).build();
        sellerEntities.add(test1);
        sellerEntities.add(test2);

        Mockito.when(sellerRepo.findAll()).thenReturn(sellerEntities);
        Mockito.when(sellerRepo.getFirstByName("Citrus")).thenReturn(Optional.of(test1));
        Mockito.when(sellerRepo.save(any(SellerEntity.class))).thenReturn(test1);
    }

    @Test
    public void getSellerByName() {
        String name = "Citrus";
        SellerDto found = sellerService.getSellerByName(name);

        Assertions.assertEquals(name, found.getName());
    }

    @Test
    public void getSellers() {
        Assertions.assertEquals(2, sellerService.getSellers().size());
    }

    @Test
    public void createSeller() {
        SellerDto test = new SellerDto();
        test.setCity("Lviv");
        SellerDto found = sellerService.createSeller(test);

        Assertions.assertEquals("Lviv", found.getCity());
    }
}