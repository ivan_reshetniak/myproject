package com.example.myprojecttest.demo.service;

import com.example.myprojecttest.demo.dto.WarehouseDto;
import com.example.myprojecttest.demo.repo.WarehouseRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
class WarehouseServiceTest {

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private WarehouseRepo warehouseRepo;

    private final WarehouseDto oldWarehouse = WarehouseDto.builder().name("warehouse11111")
            .country("Ukraine").city("Lviv").postIndex(1872).phoneNumber("55555").build();


    @Test
    public void updateWarehouseSuccessTest() {
        String oldName = oldWarehouse.getName();
        String oldPhone = oldWarehouse.getPhoneNumber();

        warehouseService.createWarehouse(oldWarehouse);
        oldWarehouse.setName("warehouse555");
        oldWarehouse.setPhoneNumber("77777");
        WarehouseDto updatedWarehouse = warehouseService.updateWarehouse(oldWarehouse.getId(), oldWarehouse);

        Assertions.assertEquals(oldWarehouse.getId(), updatedWarehouse.getId());
        Assertions.assertNotEquals(oldName, updatedWarehouse.getPhoneNumber());
        Assertions.assertNotEquals(oldPhone, updatedWarehouse.getName());
    }

    @Test
    public void updateWarehouseFailTest() {
        warehouseService.createWarehouse(oldWarehouse);
        oldWarehouse.setName(null);
        WarehouseDto updatedWarehouse = warehouseService.updateWarehouse(oldWarehouse.getId(), oldWarehouse);
        Assertions.assertNotNull(updatedWarehouse.getName());

        warehouseService.deleteWarehouse(oldWarehouse.getId());
        Assertions.assertThrows(RuntimeException.class, () -> warehouseService.updateWarehouse(oldWarehouse.getId(), oldWarehouse));
    }

    @Test
    public void deleteWarehouseTest() throws RuntimeException {
        warehouseService.createWarehouse(oldWarehouse);
        warehouseService.deleteWarehouse(oldWarehouse.getId());

        try{
            warehouseService.getWarehouseByName(oldWarehouse.getName());
        }catch (RuntimeException e){
            String expectedMessage = "Warehouse is not exist!";
            Assertions.assertEquals(expectedMessage, e.getMessage());
        }
    }

    @Test
    public void deleteWarehouseFailTest() {
        Assertions.assertThrows(RuntimeException.class, () -> warehouseService.deleteWarehouse(oldWarehouse.getId()));
    }
}