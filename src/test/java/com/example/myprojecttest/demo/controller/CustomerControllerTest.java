package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.domain.UserService;
import com.example.myprojecttest.demo.entity.CustomerEntity;
import com.example.myprojecttest.demo.repo.CustomerRepo;
import com.example.myprojecttest.demo.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
@ExtendWith(SpringExtension.class)
class CustomerControllerTest {
    @MockBean
    private CustomerService customerService;
    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private CustomerController customerController;

    @MockBean
    private CustomerRepo customerRepo;

    private ObjectMapper objectMapper = new ObjectMapper();


    @Test
    public void getCustomers() throws Exception {
        List<CustomerEntity> customerEntities = new ArrayList<>();
        CustomerEntity customer = new CustomerEntity();
        customer.setId(5);
        customer.setFirstName("Vasyl");
        customer.setLastName("Vasylenko");
        customer.setAge(35);
        customer.setMoney(5500);
        customer.setUser(null);
        customerEntities.add(customer);

        when(customerService.getAllCustomers()).thenReturn(customerEntities);

        mockMvc.perform(get("/api/customer"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(1)))
                .andExpect(jsonPath("$[0].money").value(5500));
    }

    @Test
    public void addMoney() throws Exception {
        CustomerEntity customer = new CustomerEntity();
        customer.setId(5);
        customer.setFirstName("Vasyl");
        customer.setLastName("Vasylenko");
        customer.setAge(35);
        customer.setMoney(5500);
        customer.setUser(null);

        Map<String, Long> params = new HashMap<>();
        params.put("customerId", 5L);
        params.put("money", 1000L);

        when(customerService.addMoney(anyLong(), anyInt())).thenReturn(customer);

        mockMvc.perform(put("/api/customer").content(objectMapper.writeValueAsString(params))
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.money").value(5500));
    }
}