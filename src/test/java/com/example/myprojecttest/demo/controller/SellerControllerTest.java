package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.domain.UserService;
import com.example.myprojecttest.demo.dto.SellerDto;
import com.example.myprojecttest.demo.service.SellerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import static org.mockito.Mockito.when;


@WebMvcTest(SellerController.class)
@ExtendWith(SpringExtension.class)
class SellerControllerTest {

    @MockBean
    private SellerService sellerService;

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();


    @Test
    public void getAllSellers() throws Exception {
        when(sellerService.getSellers()).thenReturn(Lists.newArrayList(
                new SellerDto().builder().id(1L).name("seller1").city("Kyiv").postIndex(111).phoneNumber("111").build(),
                new SellerDto().builder().id(2L).name("seller2").city("Kyiv").postIndex(222).phoneNumber("222").build()
        ));

        this.mockMvc.perform(get("/api/sellers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*]", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("seller1")))
                .andExpect(jsonPath("$[0].city", is("Kyiv")))
                .andExpect(jsonPath("$[0].postIndex", is(111)))
                .andExpect(jsonPath("$[0].phoneNumber", is("111")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("seller2")))
                .andExpect(jsonPath("$[1].city", is("Kyiv")))
                .andExpect(jsonPath("$[1].postIndex", is(222)))
                .andExpect(jsonPath("$[1].phoneNumber", is("222")));
    }

    @Test
    public void createSeller() throws Exception {

        SellerDto createSeller = new SellerDto().builder()
                .id(1L)
                .name("seller1")
                .city("Kyiv")
                .postIndex(111)
                .phoneNumber("111")
                .build();

        when(sellerService.createSeller(any(SellerDto.class))).thenReturn(createSeller);

        mockMvc.perform(post("/api/seller").content(objectMapper.writeValueAsString(createSeller))
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("seller1"))
                .andExpect(jsonPath("$.city").value("Kyiv"))
                .andExpect(jsonPath("$.postIndex").value(111))
                .andExpect(jsonPath("$.phoneNumber").value("111"));
    }
}