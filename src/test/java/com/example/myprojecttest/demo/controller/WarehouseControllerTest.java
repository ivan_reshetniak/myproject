package com.example.myprojecttest.demo.controller;

import com.example.myprojecttest.demo.domain.UserService;
import com.example.myprojecttest.demo.dto.WarehouseDto;
import com.example.myprojecttest.demo.service.WarehouseService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.ServletException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WarehouseController.class)
@ExtendWith(SpringExtension.class)
class WarehouseControllerTest {

    @MockBean
    private WarehouseService warehouseService;

    @MockBean
    private UserService userService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    void createWarehouse() throws Exception {
        WarehouseDto dto = new WarehouseDto();
        dto.setName("warehouse");
        dto.setCountry("Ukraine");
        dto.setCity("Kyiv");
        dto.setPostIndex(111);
        dto.setPhoneNumber("111");

        when(warehouseService.createWarehouse(any(WarehouseDto.class))).thenReturn(dto);

        mockMvc.perform(post("/api/warehouse")
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.name").value(dto.getName()))
                .andExpect(jsonPath("$.country").value(dto.getCountry()));
    }

    @Test
    public void updateWarehouseFailTest() throws Exception {
        WarehouseDto dto = new WarehouseDto();
        dto.setName("warehouse");
        dto.setCountry("Ukraine");
        dto.setCity("Kyiv");
        dto.setPostIndex(111);
        dto.setPhoneNumber("111");

        when(warehouseService.updateWarehouse(anyLong(), any(WarehouseDto.class))).thenThrow(RuntimeException.class);

        try {
            mockMvc.perform(put("/api/warehouse/5")
                    .content(objectMapper.writeValueAsString(dto))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is5xxServerError())
                    .andReturn();

        } catch (ServletException e) {
            e.getMessage();
        }
    }

    @Test
    public void updateWarehouseSuccessTest() throws Exception {
        WarehouseDto dto = new WarehouseDto();
        dto.setId(1L);
        dto.setName("warehouse");
        dto.setCountry("Ukraine");
        dto.setCity("Kyiv");
        dto.setPostIndex(111);
        dto.setPhoneNumber("111");

        when(warehouseService.updateWarehouse(anyLong(), any(WarehouseDto.class))).thenReturn(dto);

        mockMvc.perform(put("/api/warehouse/{id}", 1)
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.city").value("Kyiv"))
                .andExpect(jsonPath("$.postIndex").value(111));
    }
}